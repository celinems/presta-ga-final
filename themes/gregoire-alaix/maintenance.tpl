{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
	<title>{$meta_title|escape:'html':'UTF-8'}</title>
	<meta http-equiv="content-language" content="{$lang_iso}" />
	<meta name="language" content="{$lang_iso}" />	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	{if isset($meta_description)}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
	{/if}
	{if isset($meta_keywords)}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
	{/if}
	<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" /> 
	<meta name="apple-mobile-web-app-capable" content="yes" /> 
	<link href='http://fonts.googleapis.com/css?family=Marvel' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<meta name="robots" content="{if isset($nobots)}no{/if}index,follow" />
	<link rel="shortcut icon" href="{$favicon_url}" />
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
      <link href="{$css_dir}maintenance.css" rel="stylesheet" type="text/css" />

</head>
<body>

	<div id="wrapper">
		<div class="container">
			{$HOOK_MAINTENANCE}
			<div class="logo">
				<h1><img type="image/svg+xml" src="{$img_dir}logo-seul-b.svg" class="logo img-responsive" alt="Grégoire Alaix - sacs à main chics et décalés"/></h1>
				<!-- <h1><img src="{$img_dir}logo-seul-w-s.svg" class="logo img-responsive"></h1> -->
			</div>
			
			{l s="Website available soon! Meanwhile you can subscribe to our newsletter."}

			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup">
				<form action="http://g-alaix.us8.list-manage.com/subscribe/post?u=2b51cc4d74cc9a57163a7ba47&amp;id=9f5844feee" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<!-- <h2>{l s="Subscribe to our mailing list"}</h2> -->
				
					<div class="row">
						<div class="col-sm-4">
							<!-- <div class="mc-field-group"> -->
								<input type="email" placeholder="{l s="Email Address  "}" name="EMAIL" class="required email" id="mce-EMAIL">
							<!-- </div> -->
						</div>
						<div class="col-sm-4">
							<input type="text" placeholder="{l s="First Name "}" name="FNAME" class="" id="mce-FNAME">
						</div>
						<div class="col-sm-4">
						<!-- <div class="mc-field-group"> -->
							<input type="text" placeholder="{l s="Last Name "}" name="LNAME" class="" id="mce-LNAME">
						<!-- </div> -->
						</div>
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;"><input type="text" name="b_2b51cc4d74cc9a57163a7ba47_9f5844feee" tabindex="-1" value=""></div>
						    <div class="clear"><input type="submit" value="{l s="Subscribe"}" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
						    <!-- <div class="indicates-required"><span class="asterisk">*</span>{l s=" indicates required"}</div> -->
					</div>
				</form>
			</div>

			<!--End mc_embed_signup-->

		</div><!--  end container -->
	</div><!--  end wrapper -->
</body>
</html>