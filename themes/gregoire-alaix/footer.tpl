{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
							{if !isset($content_only) || !$content_only}
						</div> <!-- first div -->
					</div>
				</div><!-- #center_column -->
				{if isset($right_column_size) && !empty($right_column_size)}
				<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">
					<div class="center-right">
					  {$HOOK_RIGHT_COLUMN}
					</div>
				</div>
				{/if}
				</div><!-- .row -->
			</div><!-- #columns -->
		</div><!-- .columns-container -->
		{if isset($HOOK_FOOTER)}
			<!-- Footer -->
		<!-- 		<div class="footer-container">
				<footer id="footer"  class="container">
					<div class="row">{$HOOK_FOOTER}</div>
				</footer>
			</div> #footer --> 

		{/if}











		<!-- English Modal -->
		<div class="modal fade" id="newsletter-en" tabindex="-1" role="dialog" aria-labelledby="newsletter-en" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h3 class="modal-title" id="newsletter-en">Newsletter</h3>
		      </div>

		      <div class="modal-body">
		      	<!-- Begin NEW MailChimp Signup Form -->
		      	<div id="mc_embed_signup">
		      	<form action="//g-alaix.us8.list-manage.com/subscribe/post?u=2b51cc4d74cc9a57163a7ba47&amp;id=9f5844feee" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		      	    <div id="mc_embed_signup_scroll">
		      	<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
		      	<div class="mc-field-group">
		      		<label for="mce-EMAIL">E-mail  <span class="asterisk">*</span>
		      	</label>
		      		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
		      	</div>
		      	<div class="mc-field-group">
		      		<label for="mce-FNAME">First Name  <span class="asterisk">*</span>
		      	</label>
		      		<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
		      	</div>
		      	<div class="mc-field-group">
		      		<label for="mce-NOM">Last Name  <span class="asterisk">*</span>
		      	</label>
		      		<input type="text" value="" name="NOM" class="required" id="mce-NOM">
		      	</div>
		      		<div id="mce-responses" class="clear">
		      			<div class="response" id="mce-error-response" style="display:none"></div>
		      			<div class="response" id="mce-success-response" style="display:none"></div>
		      		</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		      	    <div style="position: absolute; left: -5000px;"><input type="text" name="b_2b51cc4d74cc9a57163a7ba47_9f5844feee" tabindex="-1" value=""></div>
		      	    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn"></div>
		      	    </div>
		      	</form>
		      	</div>

		      	<!--End NEW mc_embed_signup-->
		   	</div> <!-- end modal-body -->
		    </div> <!-- end modal-content -->
		 </div> <!-- end modal-dialog -->
		</div> <!-- end modal fade -->


		<!-- French Modal -->
		<div class="modal fade" id="newsletter-fr" tabindex="-1" role="dialog" aria-labelledby="newsletter-fr" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fermer</span></button>
		        <h3 class="modal-title" id="newsletter-fr">Newsletter</h3>
		      </div>

		      <div class="modal-body">
			      <!-- Begin MailChimp Signup Form -->
			      <div id="mc_embed_signup">
			      <form action="//g-alaix.us8.list-manage.com/subscribe/post?u=2b51cc4d74cc9a57163a7ba47&amp;id=22c0f78e4d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			          <div id="mc_embed_signup_scroll">
			      <div class="indicates-required"><span class="asterisk">*</span> champs requis</div>
			      <div class="mc-field-group">
			      	<label for="mce-EMAIL">Adresse e-mail  <span class="asterisk">*</span>
			      </label>
			      	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
			      </div>
			      <div class="mc-field-group">
			      	<label for="mce-FNAME">Prénom </label>
			      	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
			      </div>
			      <div class="mc-field-group">
			      	<label for="mce-LNAME">Nom  <span class="asterisk">*</span>
			      </label>
			      	<input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
			      </div>
			      	<div id="mce-responses" class="clear">
			      		<div class="response" id="mce-error-response" style="display:none"></div>
			      		<div class="response" id="mce-success-response" style="display:none"></div>
			      	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			          <div style="position: absolute; left: -5000px;"><input type="text" name="b_2b51cc4d74cc9a57163a7ba47_22c0f78e4d" tabindex="-1" value=""></div>
			          <div class="clear"><input type="submit" value="S'inscrire" name="subscribe" id="mc-embedded-subscribe" class="btn"></div>
			          </div>
			      </form>
			      </div>
			      <!--End mc_embed_signup-->
		   	</div> <!-- end modal-body -->
		    </div> <!-- end modal-content -->
		 </div> <!-- end modal-dialog -->
		</div> <!-- end modal fade -->











		</div><!-- #page -->
		{/if}
		{include file="$tpl_dir./global.tpl"}
	</body>
</html>