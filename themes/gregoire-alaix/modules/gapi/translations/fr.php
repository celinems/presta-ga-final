<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{gapi}gregoire-alaix>gapi_69ee9bf9cf3d83a8468278c44959caf0'] = 'API Google Analytics';
$_MODULE['<{gapi}gregoire-alaix>gapi_0851f7a0844553fa1168718de0f87262'] = 'Vous n\'avez pas la possibilité d\'ouvrir des URL externes';
$_MODULE['<{gapi}gregoire-alaix>gapi_6401593f1412a6b385c8e645d1f056ac'] = 'L\'extension cURL n\'est pas activée';
$_MODULE['<{gapi}gregoire-alaix>gapi_f8b94463fa8b5591e5edbbb8021e8038'] = 'OpenSSL n\'est pas activé';
$_MODULE['<{gapi}gregoire-alaix>gapi_6e4c3e76dd29876e6d33ce8c89e5fc5f'] = 'Google est inaccessible (vérifiez votre firewall)';
$_MODULE['<{gapi}gregoire-alaix>gapi_a1ed99ed6aaac91d7c3b127f032abf2d'] = 'Vous testez actuellement votre boutique sur un serveur local. Pour profiter des toutes les fonctionnalités, il vous faut mettre votre boutique sur un serveur en ligne.';
$_MODULE['<{gapi}gregoire-alaix>gapi_2ccf68a6aec8eda73156a7ef54b03351'] = 'Quelle version de l\'API Google Analytics voulez-vous utiliser?';
$_MODULE['<{gapi}gregoire-alaix>gapi_0caf30452ef28d761ae80a407b64bd9b'] = 'v1.3 : facile à configurer mais dépréciée et moins sécurisée';
$_MODULE['<{gapi}gregoire-alaix>gapi_949617ff3314c7cf2d88d356a953bd67'] = 'v3.0 : avec OAuth 2.0, la plus puissante et la plus récente';
$_MODULE['<{gapi}gregoire-alaix>gapi_f1f4f41c5cab767032db832ec7bd5b64'] = 'Enregistrer et configurer';
$_MODULE['<{gapi}gregoire-alaix>gapi_00e9b476102174b72bce85f57ef4f251'] = 'Hier, votre boutique a reçu des visites de %d personnes pour un total de %d pages vues uniques.';
$_MODULE['<{gapi}gregoire-alaix>gapi_6489ed26701b74c0fb139a3368804121'] = 'Vous devrez obtenir quelque chose comme cela';
$_MODULE['<{gapi}gregoire-alaix>gapi_6e1e99918b40cf3f46166fae1e642b73'] = 'Vous devrez maintenant avoir l\'écran suivant. Copiez/collez le \"ID client\" et le \"Secret client\" dans le formulaire ci-dessous';
$_MODULE['<{gapi}gregoire-alaix>gapi_b18cb8e83113953f96bbe47bd90ab69c'] = 'API Google Analytics v3.0';
$_MODULE['<{gapi}gregoire-alaix>gapi_76525f0f34b48475e5ca33f71d296f3b'] = 'ID client';
$_MODULE['<{gapi}gregoire-alaix>gapi_734082edf44417dd19cc65943aa65c36'] = 'Secret client';
$_MODULE['<{gapi}gregoire-alaix>gapi_cce99c598cfdb9773ab041d54c3d973a'] = 'Profil';
$_MODULE['<{gapi}gregoire-alaix>gapi_b1a026d322c634ca9e88525070e012fd'] = 'Enregistrer et se connecter à l\'API';
$_MODULE['<{gapi}gregoire-alaix>gapi_d4e6d6c42bf3eb807b8778255a4ce415'] = 'Echec lors de l\'authentification';
$_MODULE['<{gapi}gregoire-alaix>gapi_a670b4cdb42644e4b46fa857d3f73d9e'] = 'API Google Analytics v1.3';
$_MODULE['<{gapi}gregoire-alaix>gapi_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-mail';
$_MODULE['<{gapi}gregoire-alaix>gapi_dc647eb65e6711e155375218212b3964'] = 'Mot de passe';
$_MODULE['<{gapi}gregoire-alaix>gapi_970a710b7344f8639b6a86d1f081b660'] = 'Vous trouverez l\'identifiant de profil (\"Profile ID\") dans la barre d\'adresse de votre navigateur lorsque vous consultez le rapport sur le site de Google Analytics.';
$_MODULE['<{gapi}gregoire-alaix>gapi_e33d3b3409f8a0fcc326596c918c4961'] = 'Pour l\'ANCIENNE version de Google Analytics, l\'ID du profil est dans le paramètre \"id\" de l\'URL (voir \"&id=xxxxxxxx\") :';
$_MODULE['<{gapi}gregoire-alaix>gapi_c78fedea48082c7a437773e31b418f96'] = 'Pour la NOUVELLE VERSION de Google Analytics, l\'ID du profile est le numéro situé à la fin de l\'adresse, commençant par un p :';
