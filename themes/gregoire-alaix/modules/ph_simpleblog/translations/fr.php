<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_02159783610a72be7dff8c005031d7ed'] = 'Publié par';
$_MODULE['<{ph_simpleblog}gregoire-alaix>install-1.0.3_189f63f277cd73395561651753563065'] = 'Tags';
$_MODULE['<{ph_simpleblog}gregoire-alaix>install-1.0.5_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_be8df1f28c0abc85a0ed0c6860e5d832'] = 'Actualités';
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_0746618781a2a85b5edf43bd3817064b'] = 'Lien vers';
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_88a11208346ae5b4ca218d90fa29eb8b'] = 'Publié le';
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_9e2d8077785925fb583c0c202538b924'] = 'Publié dans la catégorie';
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_91401f053501b716b4a695b048c9b827'] = 'Auteur :';
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_32b502f33a535f75dcbf63f6753c631e'] = 'Tags :';
$_MODULE['<{ph_simpleblog}gregoire-alaix>list_061fcdd72d275db7234f4d3a03e3f23c'] = 'Il n\'y a pas de publication à afficher';
$_MODULE['<{ph_simpleblog}gregoire-alaix>single_be8df1f28c0abc85a0ed0c6860e5d832'] = 'Actualités';
$_MODULE['<{ph_simpleblog}gregoire-alaix>single_cc71d8f3dc8842c0f4f55e3f5b388ec6'] = 'j\'aime';
$_MODULE['<{ph_simpleblog}gregoire-alaix>single_88a11208346ae5b4ca218d90fa29eb8b'] = 'Publié le';
$_MODULE['<{ph_simpleblog}gregoire-alaix>single_9e2d8077785925fb583c0c202538b924'] = 'Publié dans la catégorie';
$_MODULE['<{ph_simpleblog}gregoire-alaix>single_91401f053501b716b4a695b048c9b827'] = 'Auteur :';
$_MODULE['<{ph_simpleblog}gregoire-alaix>single_32b502f33a535f75dcbf63f6753c631e'] = 'Tags :';
