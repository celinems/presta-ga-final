$(document).ready(function(){
  $(".home-background").backstretch([
       "/themes/g-alaix/img/sacs-accueil-1.jpg",
       "/themes/g-alaix/img/sacs-accueil-2.jpg",
        "/themes/g-alaix/img/sacs-accueil-3.jpg",
       "/themes/g-alaix/img/sacs-accueil-4.jpg",
       "/themes/g-alaix/img/sacs-accueil-5.jpg"
      ], {duration: 4000, fade: 750, slide: 'linear'});

});