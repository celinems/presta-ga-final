<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/Editing_wp-config.php Modifier
 * wp-config.php} (en anglais). C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'bastide');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'bastide');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'mfdmeo');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'mysql1.clarahost.fr');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xJdk$Z;%Fs.m&92`zRO)Q+et#/R^qKd=w~AFg5#Ee`Z&4t4a bG%>D|MlJGiW|9Q');
define('SECURE_AUTH_KEY',  '?#u(BQ]d__HcRZ&oVU+QQ9v0e>N-S3d @?m*N-volsC@e~@zqVH`U3%1m,8#h,+a');
define('LOGGED_IN_KEY',    '!58.@#<o!,8]x@=:]2&%|9_VAPT*A~C:ta1R;(+6Rwswg0H1)(&G38^=NaghHf#,');
define('NONCE_KEY',        'e+j:V,HL3!>/-4j`~2c-XqC4tN;.B6Mjl4IHb>%8zm_(=9D6Z<Vi*Qp.g,6KSjqv');
define('AUTH_SALT',        '~0O97=8W2(uQ>j?arFf*>z`Os/Aa+ILs@C*:3x#/.hk&+@(g]%%n%7!W:8.Hg-NH');
define('SECURE_AUTH_SALT', '8KS|)bmh%bQYelyM|6TFd,@F1x29Drvd9w9tt5B.#>))`C.Aa-PG{wZ!Ft{a=XHF');
define('LOGGED_IN_SALT',   'ZdQ+Y`Cf-Oh{Q?l}9C)P5mpJcun}_].rcf:n.[GaUF}-*?9)+8Hgu9(eDPd-8Yh7');
define('NONCE_SALT',       'H|U24RM-GwCa3o@d8j;iBe/jMK8?]ktH21H|E0CA[NL+:!h@<|b]QGs33=*)%;ik');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define('WPLANG', 'fr_FR');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');