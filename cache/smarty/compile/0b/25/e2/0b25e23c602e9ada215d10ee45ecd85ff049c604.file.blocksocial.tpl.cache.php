<?php /* Smarty version Smarty-3.1.14, created on 2014-09-24 21:43:03
         compiled from "/Users/celine_ms/Sites/Galaix/prestashop_1/prestashop/themes/gregoire-alaix/modules/blocksocial/blocksocial.tpl" */ ?>
<?php /*%%SmartyHeaderCode:62950868453d50829ad02c8-49001819%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b25e23c602e9ada215d10ee45ecd85ff049c604' => 
    array (
      0 => '/Users/celine_ms/Sites/Galaix/prestashop_1/prestashop/themes/gregoire-alaix/modules/blocksocial/blocksocial.tpl',
      1 => 1411587776,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '62950868453d50829ad02c8-49001819',
  'function' => 
  array (
  ),
  'cache_lifetime' => 31536000,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53d50829b368a8_37590349',
  'variables' => 
  array (
    'facebook_url' => 0,
    'twitter_url' => 0,
    'rss_url' => 0,
    'youtube_url' => 0,
    'google_plus_url' => 0,
    'pinterest_url' => 0,
    'instagram_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53d50829b368a8_37590349')) {function content_53d50829b368a8_37590349($_smarty_tpl) {?>

<section id="social_block">
	<ul>
		<?php if ($_smarty_tpl->tpl_vars['facebook_url']->value!=''){?>
			<li class="facebook">
				<a target="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facebook_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
					<span><i class="fa fa-facebook"></i></span>
				</a>
			</li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['twitter_url']->value!=''){?>
			<li class="twitter">
				<a target="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['twitter_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
					<span><i class="fa fa-twitter"></i></span>
				</a>
			</li>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['rss_url']->value!=''){?>
			<li class="rss">
				<a target="_blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rss_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
					<span><i class="fa fa-rss"></i></span>
				</a>
			</li>
		<?php }?>
        <?php if ($_smarty_tpl->tpl_vars['youtube_url']->value!=''){?>
        	<li class="youtube">
        		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['youtube_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
        			<span><i class="fa fa-youtube"></i></span>
        		</a>
        	</li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['google_plus_url']->value!=''){?>
        	<li class="google-plus">
        		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['google_plus_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
        			<span><i class="fa fa-google"></i></span>
        		</a>
        	</li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['pinterest_url']->value!=''){?>
        	<li class="pinterest">
        		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pinterest_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
        			<span><i class="fa fa-pinterest"></i></span>
        		</a>
        	</li>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['instagram_url']->value!=''){?>
          <li class="instagram">
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['instagram_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" target="_blank">
              <span><i class="fa fa-instagram"></i></span>
            </a>
          </li>
        <?php }?>
	</ul>
</section>
<div class="clearfix"></div>
<?php }} ?>