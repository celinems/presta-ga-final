<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{blockadvertising}prestashop>blockadvertising_bedd646b07e65f588b06f275bd47be07'] = 'Bloc publicités';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_dc23a0e1424eda56d0700f7ebe628c78'] = 'Ajoute un bloc affichant une publicité.';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_070e16b4f77b90e802f789b5be583cfa'] = 'Erreur de déplacement du fichier mis en ligne';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_254f642527b45bc260048e30704edb39'] = 'Paramètres';
$_MODULE['<{blockadvertising}prestashop>blockadvertising_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';


return $_MODULE;
